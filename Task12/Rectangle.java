package Task12;

class Rectangle {
    public static void main(String... args) {
        
        if(args.length == 0) {
            System.out.println("Enter row value first, then column value.");
            System.exit(0);
        } else if(args.length == 1) {
            System.out.println("Enter the second value.");
            System.exit(0);
        }

        int row = Integer.parseInt(args[0]);
        int column = Integer.parseInt(args[1]);

        for (int i = 1; i <= column; i++) {
            for (int j = 1; j <= row; j++) {
                if ((i == 1 || i == column || j == 1 || j == row)) {
                    System.out.print("# ");
                }else if ((i >= 3 && i <= column - 2 && j >= 3 && j <= (row - 2)) && (i == 3 || i == column - 2 || j == 3 || j == (row - 2))) {
                    System.out.print("# ");
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println("  ");
        }
    }
}
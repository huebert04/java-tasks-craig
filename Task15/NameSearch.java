package Task15;

import java.util.ArrayList;
import java.util.List;

class NameSearch {
    public static void main(String... args) {
        String input = args[0];

        List<Person> personList = new ArrayList<Person>();

        Person p1 = new Person("Craig", "Marais", "98789878");
        Person p2 = new Person("Dewalt", "Els", "99988098");
        Person p3 = new Person("Nicholas", "Lennox", "98099890");
        Person p4 = new Person("Kristian", "Foshaug", "99908890");
        Person p5 = new Person("Kim", "Kargaard", "99009989");

        personList.add(p1);
        personList.add(p2);
        personList.add(p3);
        personList.add(p4);
        personList.add(p5);

        for (Person person : personList) {
            if (person.getName().toLowerCase().contains(input.toLowerCase())) {
                System.out.println(person.getName());
            }
        }
    }
}
package Task15;

public class Person {
    public String fName;
    public String lName;
    public String tlf;

    public Person(String first, String last, String number) {
        fName = first;
        lName = last;
        tlf = number;
    }

    public String getTlf() {
        return tlf;
    }

    public String getName() {
        return fName + " "+ lName;
    }
}
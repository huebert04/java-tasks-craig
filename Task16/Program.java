package Task16;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

class Program {
    public static void main(String... args) throws IOException {

        if(args.length == 0) {
            System.out.println("Please have the search value!");
            System.exit(0);
        }

        String input = args[0];
        File file = new File("small.txt");
        String[] words = null;
        
        if(file.exists()) {
            double bytes = file.length();
            double kilobytes = (bytes / 1024);

            int lineCount = 0;
            int wordCount = 0;
            String s;

            FileReader fr = new FileReader(file);
            BufferedReader bf = new BufferedReader(fr);
            
            while((s=bf.readLine())!=null) {
                lineCount++;

                words = s.split(" ");

                for(String word: words) {
                    if(word.toLowerCase().contains(input.toLowerCase())) {
                        wordCount++;
                    }
                }
            }

            fr.close();
            System.out.println("File name is: " + file.getName());
            System.out.println("Your file is " + kilobytes + " kilobytes!");
            System.out.println("There are " + lineCount + " lines in the file");

            if(wordCount != 0) {
                System.out.println("The string you searched for: " + input + ", appears " + wordCount + " times in the file.");
            } else {
                System.out.println("The string you searched for: " + input + ", does not appear in the file.");
            }
        } else {
            System.out.println("File doesnt exist.");
        }
    }          
}
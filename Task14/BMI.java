package Task14;

import java.text.DecimalFormat;

class BMI {
    public static void main(String... args) {

        if(args.length == 0) {
            System.out.println("Enter the weight(kg) and height(cm)");
            System.exit(0);
        } else if(args.length == 1) {
            System.out.println("Enter the second value.");
            System.exit(0);
        }

        double weight = Integer.parseInt(args[0]);
        double height = Integer.parseInt(args[1]);
        DecimalFormat df = new DecimalFormat("####0.00");

        double bmi = (100 * 100 * weight) / (height * height);

        if(bmi < 18.5) {
            System.out.println("Your current Body Mass Index is " + df.format(bmi) + " and it is in the Underweight catergory");
        } else if(bmi >= 18.5 && bmi < 25) {
            System.out.println("Your current Body Mass Index is " + df.format(bmi) + " and it is in the Normal catergory");
        } else if(bmi >= 25 && bmi < 30 ) {
            System.out.println("Your current Body Mass Index is " + df.format(bmi) + " and it is in the Overweight catergory");
        } else if(bmi > 30) {
            System.out.println("Your current Body Mass Index is " + df.format(bmi) + " and it is in the Obese catergory");
        }
        
    }
}
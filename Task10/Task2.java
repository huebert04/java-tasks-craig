public class Task2 {
	public static void main(String... args) {
		String str = args[0];
		int count = 0;
		
		for(int i = 0; i < str.length(); i++) {
			if(str.charAt(i) != ' ') {
				count++;
			}
		}
		
		System.out.println("Hello " + str +", your name is " + count + " characters long and starts with a " + str.charAt(0));
	}
}
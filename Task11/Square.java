package Task11;

class Square {
    public static void main(String... args) {
        int length = Integer.parseInt(args[0]); // takes the first argument, converts it to int

        for (int i = 0; i < length; i++) {
            System.out.print("# ");
        }

        System.out.println("");
        for (int i = 0; i < (length - 2); i++) {
            System.out.print("# ");

            for (int j = 0; j < (length - 2); j++) {
                System.out.print("  ");
            }
            System.out.println("# ");
        }

        for (int i = 0; i < length; i++) {
            System.out.print("# ");
        }

    }
}
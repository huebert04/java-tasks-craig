package Task17;
//Cash child class that inherits the Payment parent class
public class Cash extends Payment {

    public Cash(double value) {
        super(value);
    }

    public String toString() {
        return "Your payment in cash is: " + getMoney() +"\n";
    }
}
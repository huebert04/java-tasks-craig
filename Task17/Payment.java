package Task17;

//Payment superclass whose features are inherited.
public abstract class Payment {
    public double money;

    public Payment(double money) {
        this.money = Math.round(money * 100) / 100.0;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double newMoney) {
        this.money = newMoney;
    }

    public String paymentDetails() {
        return "The payment is: " + getMoney();
    }
}
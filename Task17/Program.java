package Task17;

import java.util.Scanner;

class Program {
    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println("Please enter how much you have to pay!");
            System.exit(0);
        }

        double cost = Integer.parseInt(args[0]);
        Scanner input = new Scanner(System.in);
        System.out.println("How would you like to pay? ");
        System.out.println("1: Cash");
        System.out.println("2: Debit Card");
        System.out.println("3: Credit Card");
        int choice = input.nextInt();
        switch (choice) {
        case 1:
            Cash cash = new Cash(200);
            System.out.println(cash.toString());
            double change = cash.getMoney() - cost;
            if (cost > cash.getMoney()) {
                System.out.println("Your cash is not enough");
            } else {
                System.out.println("You payed: " + cost);
                System.out.println("Your change is: " + change);
            }
            break;
        case 2:
            DebitCard debit = new DebitCard("Kari Normann", "908890989067", 2500);
            System.out.println(debit.toString());
            double balance = debit.getMoney() - cost;
            if(cost > debit.getMoney()) {
                System.out.println("Your current balance is not enough");
            } else {
                System.out.println("You payed " + cost);
                System.out.println("Your current balance is: " + balance);
            }
            break;
        case 3:
            CreditCard credit = new CreditCard("Kari Normann", "909098900098", 1500, false);
            System.out.println(credit.toString());
            double cred = credit.getMoney() + cost;
            if(!credit.isValid()) {
                System.out.println("Your credit card is expired! Go to your bank!");
            } else {
                System.out.println("You payed " + cost);
                System.out.println("Your current credit is: " + cred);
            }
            break;
        default:
            System.out.println("Error: invalid payment method.");
        }
    }
}
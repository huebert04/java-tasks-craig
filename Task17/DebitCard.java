package Task17;
//Debit card child class that inherits the Card parent class
public class DebitCard extends Card {
    
    public DebitCard(String name, String number, double balance) {
        super(name, number, balance);
    }

    public String toString() {
        return super.cardDetails() + "\n" + "Your balance is: " + getMoney() + "\n";
    }
}
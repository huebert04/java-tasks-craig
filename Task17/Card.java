package Task17;
//Card child class that inherits the Payment parent class
public abstract class Card extends Payment {
    public String name, number;

    public Card(String name, String number, double value) {
        super(value);
        this.name = name;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String cardDetails() {
        return "Card owner: " + getName() + "\n" + 
               "Card number: " + getNumber();
    }

}
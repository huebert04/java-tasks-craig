package Task17;
//Credit card child class that inherits the Card parent class
public class CreditCard extends Card {
    public boolean valid;

    public CreditCard(String name, String number, double credit, boolean valid) {
        super(name, number, credit);
        this.valid = valid;
    }

    public boolean isValid() {
        return valid;
    }

    public String toString() {
        return super.cardDetails() + "\n" + 
               "Your total credit is: " + getMoney() + "\n";
    }
}
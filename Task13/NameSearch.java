package Task13;

import java.util.ArrayList;
import java.util.List;

class NameSearch {
    public static void main(String... args) {
        String input = args[0];

        List<String> nameList = new ArrayList<String>();

        nameList.add("Craig Marais");
        nameList.add("Dewalt Els");
        nameList.add("Nicholas Lennox");
        nameList.add("Kristian Foshaug");
        nameList.add("Kim Kargaard");

        List<String> searchResult = new ArrayList<String>();

        for (String result : nameList) {
            if (result.toLowerCase().contains(input.toLowerCase())) {
                searchResult.add(result);
            }
        }
        System.out.println(searchResult);
    }
}